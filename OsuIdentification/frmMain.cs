﻿using System;
using System.Windows.Forms;

namespace OsuIdentification {

    public partial class frmMain : Form {

        public frmMain() {
            InitializeComponent();

            // Tooltips for buttons with no words
            toolTips.SetToolTip(btnGuidEdit, "Edit your GUID");
            toolTips.SetToolTip(btnGuidHelp, "About the GUID");
            toolTips.SetToolTip(btnHwidHelp, "About the HWID");
            toolTips.SetToolTip(btnMacHelp, "About the MAC address");
        }

        // Fills the textboxes with relevant information
        private void fillInformation(object sender, EventArgs e) {
            txtHwid.Text = OsuHelper.getDiskSignature();
            txtMac.Text = OsuHelper.getNetworkInformation();
            txtGuid.Text = OsuHelper.getUninstallId();
        }

        private void openProcessList(object sender, EventArgs e) {
            frmProcessList fpl = new frmProcessList();
            fpl.ShowDialog(this);
        }

        private void openGuidEditor(object sender, EventArgs e) {
            frmGuidEditor fge = new frmGuidEditor();
            fge.ShowDialog(this);
            fillInformation(sender, e);
        }

        // All the help buttons go to here and it looks kinda messy but better than 3 methods
        private void showHelpMessages(object sender, EventArgs e) {
            string message = "implement me";
            switch(((Button)sender).Name) {
            case "btnHwidHelp":
                message = "Your HWID (or signature/uniqueid) is the identifier of your hard drive.\nYou can change it using DiskPart, as outlined in my guide at:\nquentin.sx/ban";
                break;
            case "btnGuidHelp":
                message = "Your GUID is the identifier in your registry made by osu!\nYou can change it manually in your registry or by clicking the E button";
                break;
            case "btnMacHelp":
                message = "Your MAC address identifies your network hardware.\nIt can be changed using various tools as outlined in my guide at:\nquentin.sx/ban";
                break;
            }
            MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    }

}
