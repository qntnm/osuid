﻿using System;
using System.Windows.Forms;

namespace OsuIdentification {
    static class osuID {

        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
