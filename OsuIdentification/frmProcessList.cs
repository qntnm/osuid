﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace OsuIdentification {
    public partial class frmProcessList : Form {
        public frmProcessList() {
            InitializeComponent();
        }

        private void fillProcessList(object sender, EventArgs e) {
            string tFileSizeHash, tFilename;
            FileInfo tFileInfo;
            foreach(Process p in Process.GetProcesses()) {
                try {
                    tFilename = p.MainModule.FileName;
                    tFileInfo = new FileInfo(tFilename);
                    if(tFileInfo != null)
                        tFileSizeHash = Helper.md5String(tFileInfo.Length.ToString()) + " ";
                    else
                        tFileSizeHash = "";
                } catch {
                    tFilename = "";
                    tFileSizeHash = "";
                }
                lstProcesses.Items.Add(String.Format("{0}{1} | {2} ({3})", tFileSizeHash, tFilename, p.ProcessName, p.MainWindowTitle));
            }
        }
    }
}
