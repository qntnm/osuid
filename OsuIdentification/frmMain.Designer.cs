﻿namespace OsuIdentification {
    partial class frmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblTitle1 = new System.Windows.Forms.Label();
            this.lblTitle2 = new System.Windows.Forms.Label();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlHWID = new System.Windows.Forms.GroupBox();
            this.btnHwidHelp = new System.Windows.Forms.Button();
            this.txtHwid = new System.Windows.Forms.TextBox();
            this.pnlMac = new System.Windows.Forms.GroupBox();
            this.btnMacHelp = new System.Windows.Forms.Button();
            this.txtMac = new System.Windows.Forms.TextBox();
            this.pnlGuid = new System.Windows.Forms.GroupBox();
            this.btnGuidEdit = new System.Windows.Forms.Button();
            this.btnGuidHelp = new System.Windows.Forms.Button();
            this.txtGuid = new System.Windows.Forms.TextBox();
            this.pnlProcessList = new System.Windows.Forms.GroupBox();
            this.btnViewProcessList = new System.Windows.Forms.Button();
            this.toolTips = new System.Windows.Forms.ToolTip(this.components);
            this.btnRefresh = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlTop.SuspendLayout();
            this.pnlHWID.SuspendLayout();
            this.pnlMac.SuspendLayout();
            this.pnlGuid.SuspendLayout();
            this.pnlProcessList.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(12, 4);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(393, 29);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "osu! ID Viewer";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTitle1
            // 
            this.lblTitle1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTitle1.AutoSize = true;
            this.lblTitle1.Location = new System.Drawing.Point(125, 33);
            this.lblTitle1.Name = "lblTitle1";
            this.lblTitle1.Size = new System.Drawing.Size(63, 13);
            this.lblTitle1.TabIndex = 1;
            this.lblTitle1.Text = "a BLAC tool";
            // 
            // lblTitle2
            // 
            this.lblTitle2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTitle2.AutoSize = true;
            this.lblTitle2.Location = new System.Drawing.Point(233, 33);
            this.lblTitle2.Name = "lblTitle2";
            this.lblTitle2.Size = new System.Drawing.Size(58, 13);
            this.lblTitle2.TabIndex = 2;
            this.lblTitle2.Text = "by Quentin";
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.lblTitle);
            this.pnlTop.Controls.Add(this.lblTitle2);
            this.pnlTop.Controls.Add(this.lblTitle1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(417, 52);
            this.pnlTop.TabIndex = 3;
            // 
            // pnlHWID
            // 
            this.pnlHWID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHWID.Controls.Add(this.btnHwidHelp);
            this.pnlHWID.Controls.Add(this.txtHwid);
            this.pnlHWID.Location = new System.Drawing.Point(211, 112);
            this.pnlHWID.Name = "pnlHWID";
            this.pnlHWID.Size = new System.Drawing.Size(193, 48);
            this.pnlHWID.TabIndex = 0;
            this.pnlHWID.TabStop = false;
            this.pnlHWID.Text = "HWID";
            // 
            // btnHwidHelp
            // 
            this.btnHwidHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHwidHelp.Location = new System.Drawing.Point(156, 17);
            this.btnHwidHelp.Name = "btnHwidHelp";
            this.btnHwidHelp.Size = new System.Drawing.Size(30, 23);
            this.btnHwidHelp.TabIndex = 7;
            this.btnHwidHelp.Text = "?";
            this.btnHwidHelp.UseVisualStyleBackColor = true;
            this.btnHwidHelp.Click += new System.EventHandler(this.showHelpMessages);
            // 
            // txtHwid
            // 
            this.txtHwid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHwid.BackColor = System.Drawing.Color.White;
            this.txtHwid.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHwid.Location = new System.Drawing.Point(6, 19);
            this.txtHwid.Name = "txtHwid";
            this.txtHwid.ReadOnly = true;
            this.txtHwid.Size = new System.Drawing.Size(144, 20);
            this.txtHwid.TabIndex = 6;
            // 
            // pnlMac
            // 
            this.pnlMac.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMac.Controls.Add(this.btnMacHelp);
            this.pnlMac.Controls.Add(this.txtMac);
            this.pnlMac.Location = new System.Drawing.Point(12, 142);
            this.pnlMac.Name = "pnlMac";
            this.pnlMac.Size = new System.Drawing.Size(193, 78);
            this.pnlMac.TabIndex = 1;
            this.pnlMac.TabStop = false;
            this.pnlMac.Text = "MAC";
            // 
            // btnMacHelp
            // 
            this.btnMacHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMacHelp.Location = new System.Drawing.Point(158, 19);
            this.btnMacHelp.Name = "btnMacHelp";
            this.btnMacHelp.Size = new System.Drawing.Size(30, 23);
            this.btnMacHelp.TabIndex = 4;
            this.btnMacHelp.Text = "?";
            this.btnMacHelp.UseVisualStyleBackColor = true;
            this.btnMacHelp.Click += new System.EventHandler(this.showHelpMessages);
            // 
            // txtMac
            // 
            this.txtMac.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMac.BackColor = System.Drawing.Color.White;
            this.txtMac.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMac.Location = new System.Drawing.Point(6, 19);
            this.txtMac.Multiline = true;
            this.txtMac.Name = "txtMac";
            this.txtMac.ReadOnly = true;
            this.txtMac.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMac.Size = new System.Drawing.Size(146, 52);
            this.txtMac.TabIndex = 3;
            // 
            // pnlGuid
            // 
            this.pnlGuid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlGuid.Controls.Add(this.btnGuidEdit);
            this.pnlGuid.Controls.Add(this.btnGuidHelp);
            this.pnlGuid.Controls.Add(this.txtGuid);
            this.pnlGuid.Location = new System.Drawing.Point(12, 58);
            this.pnlGuid.Name = "pnlGuid";
            this.pnlGuid.Size = new System.Drawing.Size(193, 78);
            this.pnlGuid.TabIndex = 4;
            this.pnlGuid.TabStop = false;
            this.pnlGuid.Text = "GUID";
            // 
            // btnGuidEdit
            // 
            this.btnGuidEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuidEdit.Location = new System.Drawing.Point(158, 48);
            this.btnGuidEdit.Name = "btnGuidEdit";
            this.btnGuidEdit.Size = new System.Drawing.Size(30, 23);
            this.btnGuidEdit.TabIndex = 2;
            this.btnGuidEdit.Text = "E";
            this.btnGuidEdit.UseVisualStyleBackColor = true;
            this.btnGuidEdit.Click += new System.EventHandler(this.openGuidEditor);
            // 
            // btnGuidHelp
            // 
            this.btnGuidHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuidHelp.Location = new System.Drawing.Point(158, 19);
            this.btnGuidHelp.Name = "btnGuidHelp";
            this.btnGuidHelp.Size = new System.Drawing.Size(30, 23);
            this.btnGuidHelp.TabIndex = 1;
            this.btnGuidHelp.Text = "?";
            this.btnGuidHelp.UseVisualStyleBackColor = true;
            this.btnGuidHelp.Click += new System.EventHandler(this.showHelpMessages);
            // 
            // txtGuid
            // 
            this.txtGuid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGuid.BackColor = System.Drawing.Color.White;
            this.txtGuid.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGuid.Location = new System.Drawing.Point(6, 19);
            this.txtGuid.Multiline = true;
            this.txtGuid.Name = "txtGuid";
            this.txtGuid.ReadOnly = true;
            this.txtGuid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGuid.Size = new System.Drawing.Size(146, 52);
            this.txtGuid.TabIndex = 0;
            // 
            // pnlProcessList
            // 
            this.pnlProcessList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlProcessList.Controls.Add(this.btnViewProcessList);
            this.pnlProcessList.Location = new System.Drawing.Point(211, 58);
            this.pnlProcessList.Name = "pnlProcessList";
            this.pnlProcessList.Size = new System.Drawing.Size(193, 48);
            this.pnlProcessList.TabIndex = 5;
            this.pnlProcessList.TabStop = false;
            this.pnlProcessList.Text = "Process List";
            // 
            // btnViewProcessList
            // 
            this.btnViewProcessList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewProcessList.Location = new System.Drawing.Point(6, 19);
            this.btnViewProcessList.Name = "btnViewProcessList";
            this.btnViewProcessList.Size = new System.Drawing.Size(181, 23);
            this.btnViewProcessList.TabIndex = 5;
            this.btnViewProcessList.Text = "View";
            this.btnViewProcessList.UseVisualStyleBackColor = true;
            this.btnViewProcessList.Click += new System.EventHandler(this.openProcessList);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRefresh.Location = new System.Drawing.Point(60, 8);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 38);
            this.btnRefresh.TabIndex = 6;
            this.btnRefresh.Text = "Refresh Information";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.fillInformation);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Location = new System.Drawing.Point(211, 166);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 54);
            this.panel1.TabIndex = 6;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 230);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlProcessList);
            this.Controls.Add(this.pnlGuid);
            this.Controls.Add(this.pnlHWID);
            this.Controls.Add(this.pnlMac);
            this.Controls.Add(this.pnlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(420, 269);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "osu! ID Viewer";
            this.Load += new System.EventHandler(this.fillInformation);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlHWID.ResumeLayout(false);
            this.pnlHWID.PerformLayout();
            this.pnlMac.ResumeLayout(false);
            this.pnlMac.PerformLayout();
            this.pnlGuid.ResumeLayout(false);
            this.pnlGuid.PerformLayout();
            this.pnlProcessList.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblTitle1;
        private System.Windows.Forms.Label lblTitle2;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.GroupBox pnlHWID;
        private System.Windows.Forms.TextBox txtHwid;
        private System.Windows.Forms.Button btnHwidHelp;
        private System.Windows.Forms.GroupBox pnlMac;
        private System.Windows.Forms.TextBox txtMac;
        private System.Windows.Forms.Button btnMacHelp;
        private System.Windows.Forms.GroupBox pnlGuid;
        private System.Windows.Forms.Button btnGuidEdit;
        private System.Windows.Forms.Button btnGuidHelp;
        private System.Windows.Forms.TextBox txtGuid;
        private System.Windows.Forms.GroupBox pnlProcessList;
        private System.Windows.Forms.Button btnViewProcessList;
        private System.Windows.Forms.ToolTip toolTips;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Panel panel1;
    }
}

