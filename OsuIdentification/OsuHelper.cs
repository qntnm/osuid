﻿using Microsoft.Win32;
using System;
using System.Management;
using System.Net.NetworkInformation;
using System.Text;

namespace OsuIdentification {

    /* Gets stuff just how osu! gets it */
    /* Please note that none of this was copied from osu!'s source
     * please no more copyright claims ppy bbygurl */
    public class OsuHelper {

        /* Gives your hard drive's Signature if it exists, otherwise returns the Serial Number */
        public static string getDiskSignature() {
            ManagementClass mcDisk = new ManagementClass("Win32_DiskDrive"); // Gets all the instances of DiskDrives
            foreach(ManagementObject moDisk in mcDisk.GetInstances()) {
                if(!String.IsNullOrEmpty(moDisk["Signature"].ToString()))
                    return moDisk["Signature"].ToString();
                if(!String.IsNullOrEmpty(moDisk["SerialNumber"].ToString()))
                    return moDisk["SerialNumber"].ToString();
            }
            return "0";
        }

        /* Gives your MAC address information */
        public static string getNetworkInformation() {
            StringBuilder sb = new StringBuilder();
            foreach(NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
                sb.Append(ni.GetPhysicalAddress().ToString() + ".");
            return sb.ToString();
        }

        /* Gives the UninstallID which can be used to identify you */
        public static string getUninstallId() {
            RegistryKey rkKey = Registry.CurrentUser.OpenSubKey(@"Software\osu!", false);
            if(rkKey != null && rkKey.GetValue("UninstallID") != null)
                return rkKey.GetValue("UninstallID").ToString();
            return "0";
        }

    }
}
