﻿using Microsoft.Win32;
using System;
using System.Windows.Forms;

namespace OsuIdentification {
    public partial class frmGuidEditor : Form {
        public frmGuidEditor() {
            InitializeComponent();
        }

        private void fillCurrentGuid(object sender, EventArgs e) {
            txtCurrentGuid.Text = OsuHelper.getUninstallId();
        }

        private void generateGuid(object sender, EventArgs e) {
            txtNewGuid.Text = Guid.NewGuid().ToString();
        }

        private void warnAndSave(object sender, EventArgs e) {
            if(MessageBox.Show("This will replace your current GUID!\nYou can take a copy of your current guid\nand use it later if you wish.\nAre you sure you want to overwrite your GUID?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) {
                RegistryKey rkKey = Registry.CurrentUser.OpenSubKey(@"Software\osu!", true);
                if(rkKey == null)
                    rkKey = Registry.CurrentUser.CreateSubKey(@"Software\osu!");
                rkKey.SetValue("UninstallID", txtNewGuid.Text);
                fillCurrentGuid(sender, e);
            }
        }

        private void changeSaveEnabled(object sender, EventArgs e) {
            btnSave.Enabled = !String.IsNullOrEmpty(txtNewGuid.Text);
        }

    }
}
