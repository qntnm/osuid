﻿using System.Security.Cryptography;
using System.Text;

namespace OsuIdentification {

    /* General methods for use throughout the program */
    public class Helper {

        public static string md5String(string s) {
            using(MD5 md5 = MD5.Create()) {
                byte[] baHash = md5.ComputeHash(Encoding.ASCII.GetBytes(s));
                StringBuilder sb = new StringBuilder();
                foreach(byte b in baHash)
                    sb.Append(b.ToString("x2"));
                return sb.ToString();
            }
        }

    }
}
