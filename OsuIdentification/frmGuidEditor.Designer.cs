﻿namespace OsuIdentification {
    partial class frmGuidEditor {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGuidEditor));
            this.txtCurrentGuid = new System.Windows.Forms.TextBox();
            this.lblCurrentGuid = new System.Windows.Forms.Label();
            this.lblNewGuid = new System.Windows.Forms.Label();
            this.txtNewGuid = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnGenerateGuid = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtCurrentGuid
            // 
            this.txtCurrentGuid.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentGuid.Location = new System.Drawing.Point(12, 25);
            this.txtCurrentGuid.Name = "txtCurrentGuid";
            this.txtCurrentGuid.ReadOnly = true;
            this.txtCurrentGuid.Size = new System.Drawing.Size(128, 20);
            this.txtCurrentGuid.TabIndex = 0;
            // 
            // lblCurrentGuid
            // 
            this.lblCurrentGuid.Location = new System.Drawing.Point(12, 9);
            this.lblCurrentGuid.Name = "lblCurrentGuid";
            this.lblCurrentGuid.Size = new System.Drawing.Size(128, 13);
            this.lblCurrentGuid.TabIndex = 1;
            this.lblCurrentGuid.Text = "Current GUID";
            this.lblCurrentGuid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNewGuid
            // 
            this.lblNewGuid.Location = new System.Drawing.Point(146, 9);
            this.lblNewGuid.Name = "lblNewGuid";
            this.lblNewGuid.Size = new System.Drawing.Size(128, 13);
            this.lblNewGuid.TabIndex = 3;
            this.lblNewGuid.Text = "New GUID";
            this.lblNewGuid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNewGuid
            // 
            this.txtNewGuid.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewGuid.Location = new System.Drawing.Point(146, 25);
            this.txtNewGuid.Name = "txtNewGuid";
            this.txtNewGuid.Size = new System.Drawing.Size(128, 20);
            this.txtNewGuid.TabIndex = 2;
            this.txtNewGuid.TextChanged += new System.EventHandler(this.changeSaveEnabled);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(145, 51);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(116, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save in registry";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.warnAndSave);
            // 
            // btnGenerateGuid
            // 
            this.btnGenerateGuid.Location = new System.Drawing.Point(23, 51);
            this.btnGenerateGuid.Name = "btnGenerateGuid";
            this.btnGenerateGuid.Size = new System.Drawing.Size(116, 23);
            this.btnGenerateGuid.TabIndex = 5;
            this.btnGenerateGuid.Text = "Generate GUID";
            this.btnGenerateGuid.UseVisualStyleBackColor = true;
            this.btnGenerateGuid.Click += new System.EventHandler(this.generateGuid);
            // 
            // frmGuidEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 83);
            this.Controls.Add(this.btnGenerateGuid);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblNewGuid);
            this.Controls.Add(this.txtNewGuid);
            this.Controls.Add(this.lblCurrentGuid);
            this.Controls.Add(this.txtCurrentGuid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGuidEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "GUID Editor";
            this.Load += new System.EventHandler(this.fillCurrentGuid);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCurrentGuid;
        private System.Windows.Forms.Label lblCurrentGuid;
        private System.Windows.Forms.Label lblNewGuid;
        private System.Windows.Forms.TextBox txtNewGuid;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnGenerateGuid;
    }
}